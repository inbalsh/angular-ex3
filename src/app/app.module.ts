import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//material angular
//import {MatTableModule} from '@angular/material/table'; //API Table
import {MatGridListModule} from '@angular/material/grid-list'; // API grid-list

import { AppComponent } from './app.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieComponent } from './movie/movie.component';

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    MovieComponent
  ],
  imports: [
    BrowserModule,
  //  MatTableModule  // יש להוסיף זאת לפי האימפורט למעלה
  MatGridListModule, // לפי גריד ליסט
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
