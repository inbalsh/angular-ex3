//import { Component, OnInit, Input } from '@angular/core';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'; // מחלקה שפולטת אירועים


@Component({
  selector: 'movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  @Input() data:any; // הגדרת אינפוט עם תכונה דטה
 
  @Output() myButtonClicked = new EventEmitter<any>(); // הגדרת אאוטפוט מסוג איבנט-אמיטר
  
  id;
  title;
  studio;
  weekendIncome;
  showRow = true; // הצגת השורה

  deleteRow()
  {
    console.log('event caught'); // לבדיקת טעויות
    this.myButtonClicked.emit(this.title); // לפליטת כותרת הסרט בלחיצת הכפתור
    this.showRow = false; // מחיקת השורה
  }

  constructor() { }

  ngOnInit() {
    // בעת יצירת אלמנט ליסט חדש, הפונקציה הזו רצה
    this.id = this.data.id;
    this.title = this.data.title;
    this.studio = this.data.studio;
    this.weekendIncome = this.data.weekendIncome;
    
  }

 
}
