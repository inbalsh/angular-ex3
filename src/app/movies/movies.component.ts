import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']

})
export class MoviesComponent implements OnInit {

  decmovies = [
    { 'id': '1', 'title': 'Spider-Man: Into The Spider-Verse', 'studio': 'Sony', 'weekendIncome': '$35,400,000' },
    { 'id': '2', 'title': 'The Mule', 'studio': 'WB', 'weekendIncome': '$17,210,000' },
    { 'id': '3', 'title': "Dr. Seuss' The Grinch (2018)", 'studio': 'Uni.', 'weekendIncome': '$11,580,000' },
    { 'id': '4', 'title': 'Ralph Breaks the Internet', 'studio': 'BV', 'weekendIncome': '$9,589,000' },
    { 'id': '5', 'title': 'Mortal Engines', 'studio': 'Uni.', 'weekendIncome': '$7,501,000' },
];

titleFromMovie = 'No movies were deleted';

    showTitle($event){
      this.titleFromMovie = $event;
    }

  constructor() { }

  ngOnInit() {
  }

}
